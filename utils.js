// stolen from https://jonlabelle.com/snippets/view/javascript/calculate-mean-median-mode-and-range-in-javascript i just need a function god damnit
function findMostCommon(votes) {
  let tracker = {};
  let highest = 0;
  let final = [];

  for (const [player, vote] of Object.entries(votes)) {
    let name = vote.name;
    if (typeof tracker[name] === "undefined") tracker[name] = 0;
    tracker[name] = tracker[name] + 1;
    if (tracker[name] > highest) {
      highest = tracker[name];
    }
  }

  for (const [player, h] of Object.entries(tracker)) {
    if (h >= highest) {
      final.push(player);
    }
  }

  let selected = final[Math.floor(Math.random() * final.length)];
  return ctx.clients.find((x) => {
    return x.name == selected;
  });
}

function getCount() {
  let result = {
    "Nice": 0,
    "Evil": 0
  };
  for (const [key, role] of Object.entries(ctx.libraries.Definitions.roles)) {
    let h = ctx.clients.filter((x) => {
      return x.role.name == role.name;
    });
    // total counts
    if (!role.spectator) {
      if (!role.evil) {
        result["Nice"] = result["Nice"] + h.length;
      } else {
        result["Evil"] = result["Evil"] + h.length;
      }
    }
    result[role.name] = h.length;
  }
  return result;
}

function sleep(ms) {
  var start = new Date().getTime(), expire = start + ms;
  while (new Date().getTime() < expire) { }
  return;
}

module.exports = {
  findMostCommon: findMostCommon,
  getCount: getCount,
  sleep: sleep
};

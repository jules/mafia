// timing: see https://github.com/slice/s2/blob/master/s2/exts/mafia/voting.py#L131

const roles = require("./roles");

function advanceRound() {
  ctx.round = ctx.round + 1;
  ctx.night = false;
  ctx.chatting = true;
  ctx.libraries.chat.broadcast(`Day ${ctx.round}\r\n`);
  ctx.libraries.utils.sleep(1500);
  detectEnd();
  if (ctx.round == 1) {
    gotoNight();
    return;
  }
  if (Object.keys(ctx.rolevars.killed).length !== 0) {
    ctx.libraries.chat.broadcast(
      `${ctx.rolevars.killed.name} was killed last night.\r\n`
    );
    ctx.libraries.utils.sleep(1500);
    ctx.libraries.chat.broadcast(
      `They were ${ctx.rolevars.killed.previous}.\r\n`
    );
    if (ctx.rolevars.killed.will !== "") {
      ctx.libraries.utils.sleep(1500);
      ctx.libraries.chat.broadcast(
        `Their will was: ${ctx.rolevars.killed.will}\r\n`
      );
    }
  } else if (ctx.rolevars.healed == true) {
    ctx.libraries.chat.broadcast(`Someone almost died, but was healed!\r\n`);
  } else {
    ctx.libraries.chat.broadcast(`Nobody was killed last night.\r\n`);
  }
  ctx.libraries.roles.clearVars();
  ctx.libraries.utils.sleep(3000);
  ctx.libraries.chat.broadcast(`Discuss! You have 45 seconds.\r\n`);
  setTimeout(startVote, 45000);
}

function startVote() {
  ctx.libraries.chat.broadcast(`/vote for who you want to lynch.\r\n`);
  ctx.voting = true;
  setTimeout(finishVote, 20000);
}

function finishVote() {
  if (Object.keys(ctx.rolevars.voted).length == 0) {
    ctx.libraries.chat.broadcast(`Nobody was lynched.\r\n`);
    ctx.rolevars.voted = {};
    gotoNight();
  } else {
    let target = ctx.libraries.utils.findMostCommon(ctx.rolevars.voted);
    ctx.libraries.chat.broadcast(
      `${target.name} was voted. Defend yourself!\r\n`
    );
    ctx.chatting = false;
    ctx.voting = false;
    target.trial = true;

    setTimeout(function () {
      target.trial = false;
      ctx.revoting = true;
      ctx.libraries.chat.broadcast(
        `If you believe this vote was wrong, /revote now.\r\n`
      );

      setTimeout(function () {
        ctx.revoting = false;
        let count = ctx.libraries.utils.getCount();
        let alive = ctx.clients.length - count["Dead"];

        if (Object.keys(ctx.rolevars.revote).length >= (alive / 2)) {
          // prettier-ignore
          ctx.libraries.chat.broadcast(`${Object.keys(ctx.rolevars.revote).length}/${alive} opted to revote.\r\n`);
          ctx.libraries.utils.sleep(1500);
          ctx.libraries.chat.broadcast(`Discuss! You have 45 seconds.\r\n`);
          ctx.libraries.roles.clearVars();
          ctx.chatting = true;
          setTimeout(startVote, 45000);
        } else {
          ctx.libraries.chat.broadcast(
            `${target.name} was ${target.role.name}.\r\n`
          );
          if (target.will !== "") {
            ctx.libraries.utils.sleep(1500);
            ctx.libraries.chat.broadcast(`Their will was: ${target.will}\r\n`);
          }
          killPlayer(target);
          ctx.rolevars.voted = {};
          gotoNight();
        }
      }, 10000);
    }, 20000);
  }
}

function gotoNight() {
  ctx.libraries.chat.broadcast("Night time!\r\n");
  ctx.libraries.utils.sleep(3000);
  ctx.chatting = false;
  detectEnd();
  ctx.night = true;
  ctx.libraries.chat.speakToRole(
    ctx.libraries.Definitions.roles.mafia,
    "Time to kill! Coordinate who you want to /kill, and do it.\r\n"
  );
  ctx.libraries.chat.speakToRole(
    ctx.libraries.Definitions.roles.doctor,
    "The Mafia is out killing! Coordinate who you want to /heal, and do it.\r\n"
  );

  setTimeout(function () {
    // healer
    if (Object.keys(ctx.rolevars.healed).length == 0) {
      ctx.libraries.chat.speakToRole(
        ctx.libraries.Definitions.roles.doctor,
        "Nobody was healed.\r\n"
      );
    } else {
      let target = ctx.libraries.utils.findMostCommon(ctx.rolevars.healed);
      ctx.rolevars.healed = target;
      ctx.libraries.chat.speakToRole(
        ctx.libraries.Definitions.roles.doctor,
        `${target.name} was healed.\r\n`
      );
    }

    // mafia
    if (Object.keys(ctx.rolevars.killed).length == 0) {
      ctx.libraries.chat.speakToRole(
        ctx.libraries.Definitions.roles.mafia,
        "Nobody was voted.\r\n"
      );
    } else {
      let target = ctx.libraries.utils.findMostCommon(ctx.rolevars.killed);
      let success = killPlayer(target);

      if (success) {
        ctx.rolevars.killed = target;
        ctx.libraries.chat.speakToRole(
          ctx.libraries.Definitions.roles.mafia,
          `${target.name} was killed.\r\n`
        );
      } else {
        ctx.rolevars.killed = {};
        ctx.libraries.chat.speakToRole(
          ctx.libraries.Definitions.roles.mafia,
          `${target.name} was healed!\r\n`
        );
      }
    }
    advanceRound();
  }, 36000);
}

function detectEnd() {
  let counts = ctx.libraries.utils.getCount();
  if (counts["Evil"] == 0) {
    ctx.libraries.chat.broadcast(
      `All of the Mafia are dead! The Innocents win.\r\n`
    );
    selfDestruct();
  }
  if (counts["Nice"] == 0) {
    ctx.libraries.chat.broadcast(
      `All of the Innocents are dead! The Mafia win.\r\n`
    );
    selfDestruct();
  }
  if (counts["Nice"] <= counts["Evil"]) {
    ctx.libraries.chat.broadcast(
      `The Mafia have outnumbered the Innocent! The Mafia win.\r\n`
    );
    selfDestruct();
  }
}

function killPlayer(player) {
  if (ctx.rolevars.healed.name == player.name) {
    player.connection.write(
      `You were almost killed, but a Doctor saved you!.\r\n`
    );
    ctx.rolevars.healed = true;
    return false;
  }
  player.previous = player.role.name;
  player.role = ctx.libraries.Definitions.roles.dead;
  player.dead = true;
  player.connection.write(
    `You were killed! You can still speak with other dead people, though.\r\n`
  );
  return true;
}

function selfDestruct() {
  ctx.libraries.chat.broadcast(`This game will now self destruct.\r\n`);
  ctx.clients.forEach((client) => {
    client.connection.destroy();
  });
  console.log("self destructing");
  process.exit(1);
}

module.exports = {
  advanceRound: advanceRound,
  selfDestruct: selfDestruct,
};

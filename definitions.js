const roles = {
  lobby: {
    evil: false,
    spectator: true,
    networked: false,
    name: "Lobby",
    description:
      "If you see this, something very bad has happened! Please tell me if you do.",
    weight: 0,
  },
  innocent: {
    evil: false,
    spectator: false,
    networked: false,
    name: "Innocent",
    description:
      "Figure out who you can trust and vote to kill the disguised Mafia.",
    weight: 10,
  },
  mafia: {
    evil: true,
    spectator: false,
    networked: true,
    name: "Mafia",
    description:
      "Deceive the Innocents into being trustworthy, and kill them each night.",
    weight: 0,
  },
  doctor: {
    evil: false,
    spectator: false,
    networked: true,
    name: "Doctor",
    description:
      "Work together with the Innocents and heal people to prevent attacks.",
    weight: 3,
  },
  dead: {
    evil: false,
    spectator: true,
    networked: true,
    name: "Dead",
    description:
      "You've passed away, either due to a vote or a Mafia... but can you still win?",
    weight: 0,
  },
};

class Client {
  constructor(name, connection) {
    this.name = name;
    this.connection = connection;
    this.dead = false;
    this.role = roles.lobby;
    this.admin = false;
    this.trial = false;
    this.mute = false;
    this.will = "";
    this.previous = "";
  }
}

module.exports = {
  Client: Client,
  roles: roles,
};

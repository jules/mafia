const commands = {
  start: function (c, m, a) {
    if (!c.admin) {
      c.connection.write("You are not an admin.\r\n");
      return;
    }
    if (ctx.game) {
      c.connection.write("There is a game already in progress.\r\n");
      return;
    }
    if (ctx.clients.length < 4) {
      c.connection.write(
        "Not enough players. You need " +
          ctx.clients.length -
          4 +
          " more players.\r\n"
      );
      return;
    }
    ctx.game = true;
    ctx.libraries.roles.assignRoles();
    ctx.chatting = true;
    ctx.libraries.round.advanceRound();
  },
  help: function (c, m, a) {
    c.connection.write(
      "Commands: /" + Object.keys(commands).join(" /") + "\r\n"
    );
  },
  list: function (c, m, a) {
    if (c.admin == false) {
      c.connection.write(ctx.clients.map((x) => x.name).join(", ") + "\n");
      return;
    }
    for (const [key, role] of Object.entries(ctx.libraries.Definitions.roles)) {
      c.connection.write(
        `${role.name}: ${ctx.clients
          .filter((x) => {
            return x.role.name == role.name;
          })
          .map((x) => {
            return x.name;
          })
          .join(", ")}\r\n`
      );
    }
  },
  kill: function (c, m, a) {
    if (!c.role.evil) {
      c.connection.write("You are not Mafia.\r\n");
      return;
    }
    if (ctx.night) {
      let target = findPlayer(a.join(" "));
      if (target !== undefined) {
        ctx.rolevars.killed[c.name] = target;
        c.connection.write(`You voted to kill ${target.name}.\r\n`);
      } else {
        c.connection.write(
          "Target not found. Please write their name in full.\r\n"
        );
        return;
      }
    } else {
      c.connection.write("It is not night yet.\r\n");
      return;
    }
  },
  vote: function (c, m, a) {
    if (!ctx.voting) {
      c.connection.write("It is not time to vote.\r\n");
      return;
    }
    if (c.dead) {
      c.connection.write("Dead cannot vote.\r\n");
      return;
    }
    let target = findPlayer(a.join(" "));
    if (target !== undefined) {
      if (target.dead) {
        c.connection.write(`You cannot vote a dead person.\r\n`);
        return;
      }
      ctx.rolevars.voted[c.name] = target;
      c.connection.write(`You voted to lynch ${target.name}.\r\n`);
    } else {
      c.connection.write(
        "Target not found. Please write their name in full.\r\n"
      );
      return;
    }
  },
  heal: function (c, m, a) {
    if (c.role.name !== "Doctor") {
      c.connection.write("You are not Doctor.\r\n");
      return;
    }
    if (ctx.night) {
      let target = findPlayer(a.join(" "));
      if (target.name == c.name) {
        c.connection.write("You cannot heal yourself.\r\n");
        return;
      }
      if (target !== undefined) {
        ctx.rolevars.healed[c.name] = target;
        c.connection.write(`You voted to heal ${target.name}.\r\n`);
      } else {
        c.connection.write(
          "Target not found. Please write their name in full.\r\n"
        );
        return;
      }
    } else {
      c.connection.write("It is not night yet.\r\n");
      return;
    }
  },
  eval: function (c, m, a) {
    if (!c.admin) {
      c.connection.write("No.\r\n");
      return;
    }
    let result = eval(a.join(" "));
    c.connection.write(`Result: ${result}\r\n`);
  },
  will: function (c, m, a) {
    c.will = a.join(" ");
    c.connection.write("Will set.\r\n");
  },
  revote: function (c, m, a) {
    if (c.dead) {
      c.connection.write("Dead cannot revote.\r\n");
      return;
    }
    if (!ctx.revoting) {
      c.connection.write("You cannot revote at this time.\r\n");
      return;
    }
    if (ctx.rolevars.revote[c.name] == "h") {
      c.connection.write("You have already voted for a revote.\r\n");
      return;
    }
    ctx.rolevars.revote[c.name] = "h";
    c.connection.write("Revote submitted.\r\n");
  },
};

function findPlayer(search) {
  // inefficient
  let matched = ctx.clients
    .map((client) => client.name)
    .find((name) => name.trim().match(search.trim()));
  return ctx.clients.find((client) => client.name.trim() == matched.trim());
}

function handleMessage(client, message) {
  if (message.startsWith("/")) {
    args = message.substring(1).split(" ");
    command = args.shift();
    if (commands[command] !== undefined) {
      commands[command](client, message, args);
    } else {
      client.connection.write(
        "Command not found. Type /help to see a list of commands.\r\n"
      );
    }
    return;
  }

  if (ctx.chatting == true || client.trial == true) {
    if (client.dead) {
      // prettier-ignore
      speakToRole(ctx.libraries.Definitions.roles.dead,`[${client.role.name}] <${client.name}> ${message}\r\n`);
    } else {
      broadcast(`<${client.name}> ${message}\r\n`);
    }
  } else if (
    !client.dead &&
    ctx.night == true &&
    client.role.networked == true
  ) {
    // prettier-ignore
    speakToRole(client.role, `[${client.role.name}] <${client.name}> ${message}\r\n`);
  } else {
    client.connection.write("You cannot speak at this time.\r\n");
  }
}

function broadcast(message) {
  ctx.clients.forEach((client) => {
    client.connection.write(message);
  });
}

function speakToRole(role, message) {
  ctx.clients.forEach((client) => {
    if (client.role.name == role.name || client.dead) {
      client.connection.write(message);
    }
  });
}

module.exports = {
  handleMessage: handleMessage,
  broadcast: broadcast,
  speakToRole: speakToRole,
  findPlayer: findPlayer,
};

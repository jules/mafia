const xorshift = require("./xorshift");

function findPlayersWithRoles(role) {
  return ctx.clients.filter((client) => {
    client.role = role;
  });
}

function assignRoles() {
  ctx.clients.forEach((player) => {
    player.role = ctx.libraries.Definitions.roles.innocent;
  });

  // taken from slice's code not mine lol
  let evil = Math.max(Math.min(Math.floor(ctx.clients.length / 3), 3), 1);
  let selected = [];

  // find random mafias
  while (selected.length < evil) {
    let player = ctx.clients[Math.floor(ctx.libraries.xorshift.random() * ctx.clients.length)];
    if (!selected.includes(player)) {
      selected.push(player);
    }
  }

  // assign mafia rank
  selected.forEach((player) => {
    player.role = ctx.libraries.Definitions.roles.mafia;
  });

  // weighted roles
  let unsorted = ctx.clients.filter((player) => {
    return player.role == ctx.libraries.Definitions.roles.innocent;
  })
  let weighted = [];
  Object.values(ctx.libraries.Definitions.roles).forEach((role) => {
    for (var i = 0; i < role.weight; i++) weighted.push(role);
  })
  unsorted.forEach((player) => {
    player.role = weighted[Math.floor(ctx.libraries.xorshift.random() * weighted.length)]
  })

  ctx.clients.forEach((player) => {
    player.connection.write(`You are ${player.role.name}. ${player.role.description}\r\n`);
  });
}

function clearVars() {
  for (const variable of Object.keys(ctx.rolevars)) {
    ctx.rolevars[variable] = {};
  }
}

module.exports = {
  findPlayersWithRoles: findPlayersWithRoles,
  assignRoles: assignRoles,
  clearVars: clearVars
};

// THIS CODE IS FUCKING BALLS AWFUL. a lot of this is heavily disorganized and this is like my first time doing anything like this. if you use this im gonna fcking stab you
const net = require("net");
const server = net.createServer();

// i have never done this
global.ctx = {
  clients: [],
  libraries: {
    Definitions: require("./definitions.js"),
    roles: require("./roles.js"),
    utils: require("./utils.js"),
    chat: require("./chat.js"),
    round: require("./round.js"),
    xorshift: require("./xorshift.js"),
  },
  rolevars: {
    healed: {},
    killed: {},
    voted: {},
    revote: {},
  },
  game: false,
  chatting: true,
  voting: false,
  revoting: false,
  night: false,
  round: 0,
};

server.on("connection", function (connection) {
  let registered = false;
  let msg = [];
  let client;
  connection.write("Please enter a name: ");
  connection.setEncoding("utf-8");

  connection.on("data", function (data) {
    // weird telnet issues
    if (data.endsWith("\b")) {
      msg.pop();
      return;
    }
    msg.push(data);
    if (!data.endsWith("\n")) return;

    message = msg.join("").trim();
    msg = [];

    if (!registered) {
      if (message.length > 16) {
        connection.write(`Name is too long, please pick a shorter one.\r\n`);
        return;
      }
      if (!/^([a-zA-Z0-9])+/g.test(message)) {
        connection.write(`Please provide an alphanumeric name.\r\n`);
        return;
      }
      if (ctx.clients.find((x) => x.name == message) == undefined) {
        console.log(`Player ${message} has connected`);
        client = new ctx.libraries.Definitions.Client(message, connection);

        if (ctx.clients.length === 0) {
          console.log(`Admin assigned to ${message}`);
          client.admin = true;
          connection.write(
            "You are the admin. Type '/start' to start the game.\r\n"
          );
        }

        ctx.clients.push(client);
        ctx.libraries.chat.broadcast(`${message} joined.\r\n`);
        registered = true;
      } else {
        connection.write("Name is taken, please enter a different one.\r\n");
      }
    } else {
      ctx.libraries.chat.handleMessage(client, message);
    }
  });

  function uhOh() {
    connection.destroy();
    connection.end();
    if (client.name) {
      console.log(`rip ${client.name}`);
    }
    ctx.clients.splice(ctx.clients.indexOf(client), 1);
    if (ctx.game) {
      ctx.libraries.chat.broadcast("Someone left abruptly.\r\n");
      ctx.libraries.round.selfDestruct();
    } else {
      ctx.libraries.chat.broadcast(client.name + " left.\r\n");
    }
  }

  connection.on("close", uhOh);
});

server.on("error", (error) => {
  console.log(`Server error: ${error}`);
});

server.listen(process.argv[2]);
console.log(`Tell your friends to run "nc localhost ${process.argv[2]}"!`);
